package com.mobileiron.appiumtest.tests.emailTests;

import com.mobileiron.appiumtest.pageObjects.email.MailSettingsPage;
import com.mobileiron.appiumtest.pageObjects.email.NewMailPage;
import com.mobileiron.appiumtest.tests.DefaultTest;
import com.mobileiron.appiumtest.pageObjects.email.MailsListPage;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import static junit.framework.Assert.*;

public class EmailTest extends DefaultTest {
    // TODO: 10/27/16 make it abstract

    protected static DesiredCapabilities getEmailCapabilities() {
        DesiredCapabilities capabilities = getBasicCapabilities();
        capabilities.setCapability("appWaitActivity", "com.enterproid.app.email.activity.MessageListActivity");
        capabilities.setCapability("appActivity", "com.enterproid.app.mail_zero");
        capabilities.setCapability("app", "/Users/tlozovyi/Development/EmailPlus266D.apk");
        return capabilities;
    }

    @BeforeClass
    public static void globalSetUp() throws Exception {
        startAppiumServer();
        setupDriver(getEmailCapabilities());
    }

    @AfterClass
    public static void globalTearDown() throws Exception {
        stopAppiumServer();
    }

    @Before
    public void setUp() throws Exception {
        driver.launchApp();
    }

    @After
    public void tearDown() throws Exception {
        driver.closeApp();
    }

    @Test
    public void test1() {
        String mailCc = "test@test.com";
        String mailSubject = "New mail subject";
        String mailMessage = "My new cool message to myself";
        String emailAccount = MailsListPage.getInstance(driver)
                .openSettings()
                .getAccountName();

        MailSettingsPage.getInstance(driver).pressBackButton();

        MailsListPage.getInstance(driver)
                .composeMail()
                .typeInToField(emailAccount)
                .typeSubject(mailSubject)
                .typeMessage(mailMessage)
                .closeMail(true);

        MailsListPage.getInstance(driver)
                .openFolder(MailsListPage.FOLDER.DRAFTS)
                .findMail(mailSubject, true)
                .click();

        NewMailPage.getInstance(driver)
                .typeMessage(mailMessage, true)
                .sendMail();

        boolean result = MailsListPage.getInstance(driver)
                .openFolder(MailsListPage.FOLDER.INBOX)
                .refresh()
                .openMail(mailSubject)
                .hasMessage(mailMessage);

        assertTrue(result);
    }

    @Test
    public void test2() {
        String mailCc = "test@test.com";
        String mailSubject = "New mail subject";
        String mailMessage = "My new cool message to myself";
        String emailAccount = MailsListPage.getInstance(driver)
                .openSettings()
                .getAccountName();
    }
}
