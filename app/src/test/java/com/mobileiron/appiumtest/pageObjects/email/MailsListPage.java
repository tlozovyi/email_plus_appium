package com.mobileiron.appiumtest.pageObjects.email;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.util.Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by tlozovyi on 10/18/16.
 */

public class MailsListPage extends DefaultPage {

    //toolbar elements
    private static By dropdownSpinner = byId("toolbar_spinner");
    private static By mailboxTitleTextView = By.xpath("//android.widget.Spinner[@resource-id='" + RES_PREFIX + "toolbar_spinner']" +
            "/android.widget.TextView");
    private static By searchButton = byId("search");
    private static By moreOptionsButton = By.xpath("//android.widget.ImageView[@index='1']");
    private static By refreshButton = byClassAndText(TEXT_VIEW, "Refresh");
    private static By selectAllButton = byClassAndText(TEXT_VIEW, "Select all");
    private static By deselectAllButton = byClassAndText(TEXT_VIEW, "Deselect all");
    private static By filterButton = byClassAndText(TEXT_VIEW, "Filter");
    private static By foldersButton = byClassAndText(TEXT_VIEW, "Folders");
    private static By settingsButton = byClassAndText(TEXT_VIEW, "Settings");

    //navigation spinner elements
    // TODO: 10/28/16 change to byClassAndName
    private static By inboxNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='0']");
    private static By unreadNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='1']");
    private static By attachmentNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='2']");
    private static By vipNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='3']");
    private static By draftsNavigationSpinnerElement = byClassAndText(CHECKED_TEXT_VIEW, "Drafts");
    private static By sentNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='5']");
    private static By outboxNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='6']");
    private static By trashNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='7']");
    private static By allFoldersNavigationSpinnerElement = By.xpath("//android.widget.CheckedTextView[@index='8']");

    //top notification bar
    private static By connectionErrorTextView = byId("connection_error_text");
    private static By sendingProgressSpinner = By.xpath("//android.widget.LinearLayout" +
            "[@resource-id='" + RES_PREFIX + "banner_bg']/android.widget.ProgressBar[@index='0']");
    private static By sendingTextView = By.xpath("//android.widget.LinearLayout" +
            "[@resource-id='" + RES_PREFIX + "banner_bg']/android.widget.TextView[@index='1']");

    // TODO: 10/26/16 add element and action for Send Outgoing message button in Outgoing mailbox


    //floating action button
    private static By composeButton = byId("floating_btn");

    //bottom toolbar
    private static By bottomToolbar = byId("footer_organize");
    private static By readButton = byId("btn_read");
    private static By unreadButton = byId("btn_unread");
    private static By flagButton = byId("btn_flag");
    private static By unflagButton = byId("btn_unflag");
    private static By deleteButton = byId("btn_multi_delete");
    private static By moveButton = byId("btn_multi_move");

    //listview item components
    private static By selectedMailImageView = byId("selected");
    private static By readChipImageView = byId("chip");
    private static By fromTextView = byId("from");
    private static By dateTextView = byId("date");
    private static By subjectTextView = byId("subject");
    private static By contentTextView = byId("content");
    private static By attachmentsIconImageView = byId("attachment_icon");
    private static By favoriteIconImageView = byId("favorite_icon");
    private static By conversationMessageCountTextView = byId("conversation_message_count");
    private static By importantIconImageView = byId("important_icon");
    private static By invitationIconImageView = byId("invitation_icon");
    private static By encryptedIconImageView = byId("smime_icon_encrypted");
    private static By replyStatusImageView = byId("reply_status");

    //search fields
    private static By homeButton = By.xpath("//android.view.ViewGroup" +
            "[@resource-id='" + RES_PREFIX + "toolbar']/android.widget.ImageButton[@index='0']");
    private static By searchBar = byId("search_bar");
    private static By searchFieldEditText = byId("search_src_text");
    private static By searchClearButton = byId("search_close_btn");

    //opened search fields
    private static By searchPageTitle = byId("title");
    private static By searchInFoldersButton = byId("search_scope");
    private static By searchOnServerButton = byId("main_text"); //also used for loading more message while searching on server

    //complex locators
    private static By mailBySubject(String subject) {
        return By.xpath("//*[@resource-id='" + RES_PREFIX + "subject' and contains(@text, '" + subject +"')]");
    }

    private static By checkboxMailBySubject(String subject) {
        return By.xpath("//*[@resource-id='" + RES_PREFIX + "subject' and contains(@text, '" + subject +"')]/preceding-sibling::android.widget.ImageView");
    }
    // TODO: 10/20/16 add validation that mail is checked (mb with getAttributes + hidden \ disabled

    private static By mailByPositionInVisibleList(int number) {
        return By.xpath("//*[@resource-id='" + RES_PREFIX + "rowview' and @index='" + number +
                "']//*[@resource-id='" + RES_PREFIX + "subject']");
    }

    private static By checkboxMailByPositionOnScreen(int number) {
        return By.xpath("//*[@resource-id='" + RES_PREFIX + "rowview' and @index='" + number +
                "']/descendant::android.widget.ImageView[@resource-id='" + RES_PREFIX + "selected']");
    }

    //opened filters dialog_titles
    public static final String ALL_MAIL_FILTER = "All mail";
    public static final String UNREAD_FILTER = "Unread";
    public static final String FOLLOW_UP_FILTER = "Follow-up";
    public static final String IMPORTANT_FILTER = "Important";

    private MailsListPage(AndroidDriver driver) {
        super(driver);
    }

    public static MailsListPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + MailsListPage.class.getSimpleName());
        return new MailsListPage(driver).waitForPageToBeLoaded();
    }

    @Override
    protected String expectedActivityName() {
        // TODO: 10/20/16 Add activity name
        return null;
    }

    @Override
    public MailsListPage waitForPageToBeLoaded() {
        waitFor(composeButton, DEFAULT_TIMEOUT);
        return this;
    }

    @Override
    public boolean isPageLoaded() {
        return isPresent(composeButton);
    }

    public MailFoldersPage openFoldersPageWithToolbar() {
        openMoreOptionsMenu();
        waitFor(foldersButton).click();
        delay(DEFAULT_TIMEOUT);
        return MailFoldersPage.getInstance(driver);
    }

    /**
     * Checks or unchecks email with specified subject.
     * @param subject mail with specified subject to be find
     * @return this page
     */
    public MailsListPage checkMailWithSubject(String subject) {
        waitFor(checkboxMailBySubject(subject), DEFAULT_TIMEOUT).click();
        return this;
    }

    /**
     * Checks or unchecks email with specified position on screen.
     * @param position of email on screen
     * @return this page
     */
    public MailsListPage checkMailByPositionOnScreen(int position) {
        waitFor(checkboxMailByPositionOnScreen(position), DEFAULT_TIMEOUT).click();
        return this;
    }

    public MailsListPage deleteMailWithSubject(String subject) {
        deselectAllMails();
        checkMailWithSubject(subject);
        try {
            waitFor(deleteButton, DEFAULT_TIMEOUT).click();
        } catch (TimeoutException e) {
            fail("Delete button didn't appeared.");
        }
        return this;
    }

    public MailsListPage markMailRead(String subject) {
        writeLog("Mark mail with subject: '" + subject + "' as read");
        deselectAllMails();
        checkMailWithSubject(subject);
        waitFor(readButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public MailsListPage markMailUnread(String subject) {
        writeLog("Mark mail with subject: '" + subject + "' as unread");
        deselectAllMails();
        checkMailWithSubject(subject);
        waitFor(unreadButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public MailsListPage flagMail(String subject) {
        writeLog("Flag mail with subject: '" + subject + "'");
        deselectAllMails();
        checkMailWithSubject(subject);
        waitFor(flagButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public MailsListPage unflagMail(String subject) {
        writeLog("Unflag mail with subject: '" + subject + "'");
        deselectAllMails();
        checkMailWithSubject(subject);
        waitFor(unflagButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public int getMailPositionBySubject(String subject) {
        writeLog("Looking for position of mail with subject: " + subject);
        List subjectViewsList = driver.findElements(subjectTextView);
        for (int i = 0; i < subjectViewsList.size(); i++) {
            if (subjectViewsList.get(i) == null) {
                break;
            }
            writeLog(((WebElement) subjectViewsList.get(i)).getText());
            if (((WebElement) subjectViewsList.get(i)).getText().contains(subject)) {
                writeLog("Position is: " + (i + 1));
                return i + 1;
            }
        }
        return -1;
    }

    public NewMailPage pressComposeButton() {
        driver.findElement(composeButton).click();
        return NewMailPage.getInstance(driver);
    }

    private void openMoreOptionsMenu() {
        driver.findElement(moreOptionsButton).click();
    }

    /**
     * Opens more options in toolbar and clicks on Deselect all button if it is present.
     * @return this page
     */
    public MailsListPage deselectAllMails() {
        writeLog("Deselect all mails");
        openMoreOptionsMenu();
        if (!tryClick(tryFind(deselectAllButton))) {
            pressBackButton();
        }
        return this;
    }

    //navigation between mailboxes
    //########################################################
    public enum FOLDER {
        INBOX(inboxNavigationSpinnerElement),
        UNREAD(unreadNavigationSpinnerElement),
        ATTACHMENTS(attachmentNavigationSpinnerElement),
        VIP(vipNavigationSpinnerElement),
        DRAFTS(draftsNavigationSpinnerElement),
        SENT(sentNavigationSpinnerElement),
        OUTBOX(outboxNavigationSpinnerElement),
        TRASH(trashNavigationSpinnerElement),
        ALL_FOLDERS(allFoldersNavigationSpinnerElement);

        private By locator;

        FOLDER(By locator) {
            this.locator = locator;
        }

        public By getLocator() {
            return locator;
        }
    }

    /**
     * Opens one ofe folders displayed in dropdown spinner
     * @param folder to be opened
     * @return this page
     */
    public MailsListPage openFolder(FOLDER folder) {
        writeLog("Opening " + folder.name() + " folder");
        waitFor(dropdownSpinner).click();
        waitFor(folder.getLocator()).click();
        return this;
    }

    /**
     * Opens folder with specified name. If folder is present in dropdown spinner list -
     * it will be opened from there. If not - folders page will be opened {@link MailFoldersPage}.
     * If folder with specified name is present - it will be opened, if not -
     * Inbox folder will be opened instead.
     * @param folder
     * @return
     */
    public MailsListPage openFolder(String folder) {
        waitFor(dropdownSpinner);
        if (!getCurrentMailboxTitle().equals(folder)) {
            waitFor(dropdownSpinner).click();
            if (isPresent(elementOfDialogList(folder)))
                waitFor(elementOfDialogList(folder)).click();
            else {
                pressBackButton();
                MailFoldersPage mailFoldersListPage = openFoldersPageWithToolbar();
                return mailFoldersListPage.openFolder(folder);
            }
        }
        return this;
    }

    public String getCurrentMailboxTitle() {
        assertTrue(waitFor(mailboxTitleTextView) != null);
        return driver.findElement(mailboxTitleTextView).getText();
    }

    public MailsListPage moveMailWithSubjectTo(String subject, String destination) {
        deselectAllMails() ;
        checkMailWithSubject(subject);
        waitFor(moveButton, DEFAULT_TIMEOUT).click();
        waitFor(elementOfDialogList(destination), DEFAULT_TIMEOUT).click();
        return this;
    }

    public MailsListPage refresh() {
        writeLog("Refreshing");
        openMoreOptionsMenu();
        waitFor(refreshButton).click();
        delay(1);
        return this;
    }

    public MailsListPage refreshPullDown() {
        scroll(DIRECTION_UP);
        delay(DEFAULT_TIMEOUT);
        return this;
    }


    public MailsListPage typeInSearchField(String query) {
        waitFor(searchButton).click();
        waitFor(searchFieldEditText).sendKeys(query);
        delay(2);
        return this;
    }

    public MailsListPage searchFor(String query) {
        typeInSearchField(query);
        driver.pressKeyCode(AndroidKeyCode.ENTER);
        delay(DEFAULT_TIMEOUT);
        return MailsListPage.getInstance(driver);
    }

    public MailsListPage clearSearchField() {
        waitFor(searchClearButton).click();
        delay(2);
        return this;
    }

    public MailsListPage closeSearchMode() {
        waitFor(homeButton).click();
        delay(2);
        return this;
    }

    /**
     * Applies specified filter to list of mails
     * @param filter can be {@link ALL_MAIL_FILTER}, {@link UNREAD_FILTER}, {@link FOLLOW_UP_FILTER} or {@link IMPORTANT_FILTER}
     * @return this page
     */
    public MailsListPage applyFilter(String filter) {
        openMoreOptionsMenu();
        waitFor(filterButton, DEFAULT_TIMEOUT).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, filter), DEFAULT_TIMEOUT).click();
        return this;
    }

    /**
     * Opens thread by subject if it is present on screen, fails test otherwise.
     * Also fails test at attempt to open simple mail (not conversation).
     * @param subject to search for
     * @return this page
     */
    public MailsListPage openThread(String subject) {
        int index = getMailPositionBySubject(subject);
        By threadIcon = By.xpath("//*[@resource-id='com.mobileiron.android.pim:id/rowview' and @index='" + index + "']//" +
                "android.widget.TextView[@resource-id='com.mobileiron.android.pim:id/conversation_message_count']");
        waitFor(threadIcon); //check that it is conversation
        waitFor(mailBySubject(subject), DEFAULT_TIMEOUT).click();
        return MailsListPage.getInstance(driver);
    }

    public MailsListPage closeThread() {
        waitFor(homeButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public boolean hasMailWithSubject(String subject) {
        return isPresent(mailBySubject(subject));
    }

    public boolean isMessageRead(String subject) {
        deselectAllMails();
        checkMailWithSubject(subject);
        boolean present = isPresent(unreadButton);
        checkMailWithSubject(subject); //uncheck
        return present;
    }

    public boolean isMessageUnread(String subject) {
        return !isMessageRead(subject);
    }

    public boolean isMessageFlagged(String subject) {
        deselectAllMails();
        checkMailWithSubject(subject);
        boolean present = isPresent(unflagButton);
        checkMailWithSubject(subject); //uncheck
        return present;

    }

    public boolean isMessageUnflagged(String subject) {
        return !isMessageFlagged(subject);
    }

    public int letterByMask(String mask) {
        return driver.findElements(mailBySubject(mask)).size();
    }

    public ViewMailPage openMail(String subject) {
        // TODO: 10/28/16 open new mail page object from drafts
        writeLog("Opening mail with subject: " + subject);
        waitFor(mailBySubject(subject), DEFAULT_TIMEOUT).click();
        delay(DEFAULT_TIMEOUT);
        return ViewMailPage.getInstance(driver);
    }

    public ViewMailPage openMail(int position) {
        waitFor(mailByPositionInVisibleList(position), DEFAULT_TIMEOUT).click();
        return ViewMailPage.getInstance(driver);
    }

    public void deleteAllMailOnScreenByPattern(String pattern) {
        while (isPresent(mailBySubject(pattern))) {
            deleteMailWithSubject(pattern);
        }
    }

    public NewMailPage composeMail() {
        writeLog("Starting composing mail");
        waitFor(composeButton, DEFAULT_TIMEOUT).click();
        return NewMailPage.getInstance(driver);
    }

    public MailSettingsPage openSettings() {
        writeLog("Opening settings");
        openMoreOptionsMenu();
        waitFor(settingsButton, DEFAULT_TIMEOUT).click();
        return MailSettingsPage.getInstance(driver);
    }

    public String getSubjectOfLastMailOnScreen() {
        List subjects = driver.findElements(subjectTextView);
        return ((WebElement) subjects.get(subjects.size() - 1)).getText();
    }

    public String getContentOfLastMailOnScreen() {
        List subjects = driver.findElements(contentTextView);
        return ((WebElement) subjects.get(subjects.size() - 1)).getText();
    }

    public WebElement findMail(String subject, boolean scroll) {
        writeLog("Trying to find mail with subject: " + subject);
        By locator = mailBySubject(subject);
        if (isPresent(locator)) {
            return waitFor(locator);
        }
        if (scroll) {
            WebElement row = driver.findElement(By.xpath("//*[@resource-id='com.mobileiron.android.pim:id/rowview']"));
            int height = row.getSize().height;
            String lastRowSubject = null;
            String lastRowContent = null;
            while (true) {
                scroll(DIRECTION_DOWN, INVALID_VALUE, INVALID_VALUE, 0, height * 4);
                if (isPresent(locator)) {
                    return waitFor(locator);
                }
                String lastSubject = getSubjectOfLastMailOnScreen();
                String lastContent = getContentOfLastMailOnScreen();
                if (lastSubject.equals(lastRowSubject) && lastContent.equals(lastRowContent)) {
                    return null;
                }
                lastRowSubject = lastSubject;
                lastRowContent = lastContent;
            }
        }
        return null;
    }

    public MailsListPage scrollToBottom() {
        scrollToBottom(mailboxTitleTextView);
        return MailsListPage.getInstance(driver);
    }

    /**
     * @return true if local, false if server search
     */
    public boolean isLocalSearch() {
        return waitFor(searchPageTitle).getText().toLowerCase().contains("local");
    }

    public MailsListPage searchOnServer() {
        waitFor(searchOnServerButton).click();
        return MailsListPage.getInstance(driver);
    }

    public MailsListPage loadMoreElements() {
        waitFor(searchOnServerButton).click();
        return MailsListPage.getInstance(driver);
    }

}
