package com.mobileiron.appiumtest.pageObjects.email;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.util.Helpers;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by tlozovyi on 10/21/16.
 */

public class NewMailPage extends DefaultPage {

    private static By homeButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    private static By pageTitle = byId("title");

    //toolbar
    private static By encryptButton = byId("smime_encryt_sign");
    private static By priorityButton = byId("set_priority");
    private static By addAttachmentButton = byId("add_attachment");
    private static By sendButton = byId("send");

    //attachments
    private static By attachmentName = byId("attachment_name");
    private static By attachmentDeleteButton = byId("attachment_delete");

    //composing
    private static By toField = byId("to");
    private static By ccbccField = byId("ccbcc");
    private static By ccField = byId("cc");
    private static By bccField = byId("bcc");
    private static By subjectEditText = byId("subject");
    private static By messageContentEditText = byId("message_content");

    private NewMailPage(AndroidDriver driver) {
        super(driver);
    }

    public static NewMailPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + NewMailPage.class.getSimpleName());
        return new NewMailPage(driver).waitForPageToBeLoaded();
    }

    @Override
    protected String expectedActivityName() {
        // TODO: 10/21/16  
        return null;
    }

    @Override
    public NewMailPage waitForPageToBeLoaded() {
        if (waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Compose")) {
            return this;
        }
        Assert.fail();
        return null;
    }

    @Override
    public boolean isPageLoaded() {
        return waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Compose");
    }

    public NewMailPage changeMailSecurity(boolean encryptOn, boolean signOn) {
        writeLog("Turning encryption " + (encryptOn ? "on" : "off") + " and sign " + (signOn ? "on" : "off"));
        waitFor(encryptButton).click();
        WebElement encryptElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "Encrypt"));
        WebElement signElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "Sign"));
        if (encryptElement.getAttribute("checked").equals("true") != encryptOn) {
            encryptElement.click();
        }
        if (signElement.getAttribute("checked").equals("true") != signOn) {
            signElement.click();
        }
        closeDialogWithPositiveButton();
        return waitForPageToBeLoaded();
    }

    public boolean isEncriptionOn() {
        waitFor(encryptButton).click();
        WebElement encryptElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "Encrypt"));
        boolean result = encryptElement.getAttribute("checked").equals("true");
        closeDialogWithNegativeButton();
        return result;
    }

    public boolean isSignOn() {
        waitFor(encryptButton).click();
        WebElement signElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "Sign"));
        boolean result = signElement.getAttribute("checked").equals("true");
        closeDialogWithNegativeButton();
        return result;
    }

    public class Priority {
        public static final String LOW = "Low";
        public static final String NORMAL = "Normal";
        public static final String HIGH = "High";
    }

    public NewMailPage changePrioity(String priority) {
        writeLog("Changing priority to " + priority);
        waitFor(priorityButton).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, priority)).click();
        return this;
    }

    public String getPriority() {
        waitFor(priorityButton).click();
        String result = waitFor(By.xpath("//android.widget.CheckedTextView[@checked='true']")).getText();
        pressBackButton();
        return result;
    }

    public NewMailPage addAttachments(int numberOfAttachments) {
        writeLog("Adding " + numberOfAttachments + " attachments.");
        for (int i = 0; i < numberOfAttachments; i++) {
            waitFor(addAttachmentButton).click();
            waitFor(byClassAndText(TEXT_VIEW, "Downloads")).click();
            List icons = driver.findElements(By.id("com.android.documentsui:id/icon_thumb"));
            ((WebElement) icons.get(i % icons.size())).click();
        }
        return waitForPageToBeLoaded();
    }

    public List<String> getAttachmentsNames() {
        List<String> names = new ArrayList<>();
        String lastName = null;
        while (true) {
            List elements = driver.findElements(attachmentName);
            for (int i = 0; i < elements.size(); i++) {
                String name = ((WebElement) elements.get(i)).getText();
                if (!names.contains(name)) {
                    names.add(name);
                }
            }
            String lastVisibleName = ((WebElement) elements.get(elements.size() - 1)).getText();
            if (lastVisibleName == null || lastVisibleName.equals(lastName)) {
                break;
            }
            lastName = lastVisibleName;
            scroll(DIRECTION_DOWN);
        }
        return names;
    }

    public NewMailPage removeAllAttachments() {
        writeLog("Removing all attachments");
        while (isPresent(attachmentDeleteButton)) {
            waitFor(attachmentDeleteButton).click();
        }
        return waitForPageToBeLoaded();
    }

    public NewMailPage removeAttachment(String name) {
        writeLog("Removing attachment with name " + name);
        waitFor(By.xpath("//*[@text='" + name + "']" +
                "/following-sibling::android.widget.FrameLayout" +
                "/android.widget.ImageButton")).click();
        return waitForPageToBeLoaded();
    }

    public void sendMail() {
        writeLog("Sending mail");
        waitFor(sendButton).click();
    }

    public NewMailPage typeInToField(String to) {
        return typeInToField(to, false);
    }

    public NewMailPage typeInToField(String to, boolean clear) {
        writeLog("Typing in to filed: " + to);
        typeIn(toField, to, clear);
        driver.pressKeyCode(AndroidKeyCode.ENTER);
        return waitForPageToBeLoaded();
    }

    public String getToFieldContent() {
        return waitFor(toField).getText();
    }

    public NewMailPage typeInCcField(String cc) {
        return typeInCcField(cc, false);
    }

    public NewMailPage typeInCcField(String cc, boolean clear) {
        writeLog("Typing in cc filed: " + cc);
        if (isPresent(ccbccField)) {
            typeIn(ccbccField, cc, clear);
            driver.pressKeyCode(AndroidKeyCode.ENTER);
        } else {
            typeIn(ccbccField, cc, clear);
            driver.pressKeyCode(AndroidKeyCode.ENTER);
        }
        return waitForPageToBeLoaded();
    }

    public String getCcFieldContent() {
        if (isPresent(ccbccField)) {
            return waitFor(ccbccField).getText();
        } else {
            return waitFor(ccField).getText();
        }

    }

    public NewMailPage typeInBccField(String cc) {
        return typeInBccField(cc, false);
    }

    public NewMailPage typeInBccField(String cc, boolean clear) {
        writeLog("Typing in bcc filed: " + cc);
        typeIn(bccField, cc, clear);
        driver.pressKeyCode(AndroidKeyCode.ENTER);
        return waitForPageToBeLoaded();
    }

    public String getBccFieldContent() {
        return waitFor(bccField).getText();
    }

    public NewMailPage typeSubject(String subject) {
        return typeSubject(subject, false);
    }

    public NewMailPage typeSubject(String subject, boolean clear) {
        writeLog("Typing in subject filed: " + subject);
        while (!isPresent(subjectEditText)) {
            scroll(DIRECTION_DOWN);
        }
        if (clear) {
            Helpers.sendText(" ");
            forceClear(subjectEditText);
        }
        typeIn(subjectEditText, subject, false);
        return waitForPageToBeLoaded();
    }

    public String getSubjectFieldContent() {
        while (!isPresent(subjectEditText)) {
            scroll(DIRECTION_DOWN);
        }
        return waitFor(subjectEditText).getText();
    }

    public NewMailPage typeMessage(String message) {
        return typeMessage(message, false);
    }

    public NewMailPage typeMessage(String message, boolean clear) {
        writeLog("Typing message: " + message);
        while (!isPresent(subjectEditText)) {
            scroll(DIRECTION_DOWN);
        }
        waitFor(subjectEditText).click();
        driver.pressKeyCode(AndroidKeyCode.ENTER);
        if (!clear) {
            Helpers.sendText(message);
        } else {
            typeIn(messageContentEditText, message, true);
        }
        return waitForPageToBeLoaded();
    }

    public String getMessageFieldContent() {
        while (!isPresent(messageContentEditText)) {
            scroll(DIRECTION_DOWN);
        }
        return waitFor(messageContentEditText).getText();
    }

    public void closeMail(boolean save) {
        writeLog("Closing " + (save ? "and saving " : "") + "mail");
        waitFor(homeButton).click();
        if (save) {
            closeDialogWithPositiveButton();
        } else {
            closeDialogWithNegativeButton();
        }
    }

}
