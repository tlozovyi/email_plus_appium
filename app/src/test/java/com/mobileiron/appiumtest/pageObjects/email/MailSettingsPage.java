package com.mobileiron.appiumtest.pageObjects.email;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.util.Helpers;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by tlozovyi on 10/21/16.
 */

public class MailSettingsPage extends DefaultPage {

    private static By pageTitle = By.xpath("//android.view.View[@resource-id='com.mobileiron.android.pim:id/action_bar']" +
            "/android.widget.TextView[@text='Settings']");

    //fields with values
    private static By accountNameField = getSettingFieldValue("Account name");
    private static By currentUsernameField = getSettingFieldValue("Your name");
    private static By amountToSyncField = getSettingFieldValue("Amount to synchronize");
    private static By signatureField = getSettingFieldValue("Signature");
    private static By emailCheckFrequencyField = getSettingFieldValue("Email check frequency");
    private static By showPicturesField = getSettingFieldValue("Show pictures");
    private static By textSizeField = getSettingFieldValue("Text size");
    private static By autoAdvanceField = getSettingFieldValue("Auto-advance");
    private static By textPreviewField = getSettingFieldValue("Text preview");
    private static By volumeKeyBehaviorField = getSettingFieldValue("Volume key behavior");
    private static By silentIntervalBetweenNotificationsField = getSettingFieldValue("Silent interval between notifications");
    private static By mailNotificationsField = getSettingFieldValue("Mail notifications");
    private static By soundField = getSettingFieldValue("Sound");
    private static By vibrateField = getSettingFieldValue("Vibrate");
    private static By buildVersionField = getSettingFieldValue("Build version");
    private static By easDeviceIdField = getSettingFieldValue("EAS device ID");

    //buttons
    private static By emailAliasesButton = byClassAndText(TEXT_VIEW, "Email aliases");
    private static By outOfOfficeButton = byClassAndText(TEXT_VIEW, "Out of office");
    private static By serverSettingsButton = byClassAndText(TEXT_VIEW, "Server settings");
    private static By keystoreButton = byClassAndText(TEXT_VIEW, "Keystore");
    private static By mailSecurity = byClassAndText(TEXT_VIEW, "Mail security");
    private static By restoreDefaultsButton = byClassAndText(TEXT_VIEW, "Restore defaults");
    private static By termsOfServiceButton = byClassAndText(TEXT_VIEW, "Email+ Terms of Service");

    //switches
    private static By confirmBeforeDeletingSwitch = getSettingSwitch("Confirm before deleting");
    private static By useSmartSendSwitch = getSettingSwitch("Use smart send");
    private static By signInAutoRetrySwitch = getSettingSwitch("Sign-in auto retry");
    private static By emailThreadingSwitch = getSettingSwitch("Email threading");
    private static By swipeActionsSwitch = getSettingSwitch("Swipe actions");
    private static By detailedNotificationsSwitch = getSettingSwitch("Detailed notifications");  //disabled
    private static By emailSentNotificationSwitch = getSettingSwitch("Email sent notification");

    //email aliases page
    private static By addEmailAliasesButton = byId("add_alias_account");
    private static By emailAliasTextView = byId("email");
    private static By deleteAliasButton = byId("delete");
    private static By emailAliasTextByPosition(int position) {
        return  By.xpath("//android.widget.TextView[resource-id='listview']" +
                "/android.widget.RelativeLayout[index='" + position + "']/*[resource-id='email'");
    }
    private static By deleteEmailButtonByAlias(String alias) {
        return  By.xpath("//android.widget.TextView[@text='" + alias + "']" +
                "/following-sibling::android.widget.ImageButton");
    }

    //out of office page
    private static By saveOutOfOfficeButton = byId("save");
    private static By cancelOutOfOfficeButton = byId("cancel");
    private static By outOfOfficeSpinner = byId("oof_status");
    private static By outOfOfficeSpinnerTextView = By.xpath("//*[resource-id='com.mobileiron.android.pim:id/oof_status']/android.widget.TextView");
    private static By outOfOfficeEditText = byId("oof_message");
    private static By outOfOfficeKnownUsers = byId("oof_appliesToExternalKnownUser");
    private static By outOfOfficeUnknownUsers = byId("oof_appliesToExternalUnknownUser");
    private static By outOfOfficeStartDateButton = byId("oof_start_date");
    private static By outOfOfficeStartTimeButton = byId("oof_start_time");
    private static By outOfOfficeEndDateButton = byId("oof_end_date");
    private static By outOfOfficeEndTimeButton = byId("oof_end_time");
    ///////////////////


    private static By listViewElementTitle = byAndroidId("title");
    private static By editTextFieldInDialog = byAndroidId("edit");

    private static By getSettingFieldValue(String settingNAme) {
        return  By.xpath("//android.widget.TextView[@text='" +settingNAme + "']/following-sibling::android.widget.TextView");
    }

    private static By getSettingSwitch(String settingNAme) {
        return  By.xpath("//android.widget.TextView[@text='" +settingNAme + "']/following-sibling::android.widget.Switch");
    }

    private MailSettingsPage(AndroidDriver driver) {
        super(driver);
    }

    public static MailSettingsPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + MailSettingsPage.class.getSimpleName());
        return new MailSettingsPage(driver).waitForPageToBeLoaded();
    }
    
    @Override
    protected String expectedActivityName() {
        // TODO: 10/21/16  
        return null;
    }

    @Override
    public MailSettingsPage waitForPageToBeLoaded() {
        if (waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Settings")) {
            return this;
        }
        Assert.fail();
        return null;
    }

    @Override
    public boolean isPageLoaded() {
        return waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Settings");
    }

    private WebElement findSetting(By locator) {
        boolean scrollDown = true;
        String lastElementTitle = null;
        while (!isPresent(locator)) {
            List listViewTitles = driver.findElements(listViewElementTitle);
            if (scrollDown) {
                if (((WebElement) listViewTitles.get(listViewTitles.size() - 1)).getText().equals(lastElementTitle)) {
                    scrollDown = false;
                } else {
                    lastElementTitle = ((WebElement) listViewTitles.get(listViewTitles.size() - 1)).getText();
                    scroll(DIRECTION_DOWN);
                }
            } else {
                if (((WebElement) listViewTitles.get(0)).getText().equals(lastElementTitle)) {
                    Assert.fail();
                } else {
                    lastElementTitle = ((WebElement) listViewTitles.get(0)).getText();
                    scroll(DIRECTION_UP);
                }
            }
        }

        return waitFor(locator);
    }

    //########################################
    //fields
    //########################################

    public String getAccountName() {
        return findSetting(accountNameField).getText();
    }

    public String getCurrentUserName() {
        return findSetting(currentUsernameField).getText();
    }

    public MailSettingsPage setCurrentUserName(String userName) {
        findSetting(currentUsernameField).click();
        waitFor(editTextFieldInDialog);
        typeIn(editTextFieldInDialog, userName, true);
        closeDialogWithPositiveButton();
        return this;
    }

    public class AmountToSychronize {
        public static final String ONE_DAY = "One day";
        public static final String THREE_DAYS = "Three days";
        public static final String ONE_WEEK = "One week";
        public static final String TWO_WEEKS = "Two weeks";
        public static final String ONE_MONTH = "One month";
        public static final String ALL_MAILS = "All mails";
    }

    public String getAmountToSyncValue() {
        return findSetting(amountToSyncField).getText();
    }

    public MailSettingsPage setAmountToSync(String value) {
        findSetting(amountToSyncField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public String getSignature() {
        return findSetting(signatureField).getText();
    }

    public MailSettingsPage setSignature(String signature) {
        findSetting(signatureField).click();
        waitFor(editTextFieldInDialog);
        typeIn(editTextFieldInDialog, signature, true);
        closeDialogWithPositiveButton();
        return this;
    }

    public class EmailCheckFrequency {
        public static final String AUTOMATIC_PUSH = "Automatic (Push)";
        public static final String NEVER = "Never";
        public static final String EVERY_5_MINUTES = "Every 5 minutes";
        public static final String EVERY_10_MINUTES = "Every 10 minutes";
        public static final String EVERY_15_MINUTES = "Every 15 minutes";
        public static final String EVERY_30_MINUTES = "Every 30 minutes";
        public static final String EVERY_HOUR = "Every hour";
    }

    public String getEmailCheckFrequencyValue() {
        return findSetting(emailCheckFrequencyField).getText();
    }

    public MailSettingsPage setEmailCheckFrequency(String value) {
        findSetting(emailCheckFrequencyField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class ShowPictures {
        public static final String NEVER = "Never";
        public static final String ON_WIFI_ONLY = "On Wi-Fi only";
        public static final String ALWAYS = "Always";
    }

    public String getShowPicturesValue() {
        return findSetting(showPicturesField).getText();
    }

    public MailSettingsPage setShowPictures(String value) {
        findSetting(showPicturesField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class TextSize {
        public static final String NORMAL = "Normal";
        public static final String LARGE = "Large";
        public static final String EXTRA_LARGE = "Extra Large";
    }

    public String getTextSizeValue() {
        return findSetting(textSizeField).getText();
    }

    public MailSettingsPage setTextSize(String value) {
        findSetting(textSizeField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class AutoAdvance {
        public static final String OLDER = "Older";
        public static final String NEWER = "Newer";
        public static final String MESSAGE_LIST = "Message list";
    }

    public String getAutoAdvanceValue() {
        String value = findSetting(textSizeField).getText();
        if (value.contains(AutoAdvance.OLDER.toLowerCase())) {
            return AutoAdvance.OLDER;
        }
        if (value.contains(AutoAdvance.NEWER.toLowerCase())) {
            return AutoAdvance.NEWER;
        }
        if (value.contains(AutoAdvance.MESSAGE_LIST.toLowerCase())) {
            return AutoAdvance.MESSAGE_LIST;
        }
        return null;
    }

    public MailSettingsPage setAutoAdvance(String value) {
        findSetting(autoAdvanceField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class TextPreview {
        public static final String DISABLED = "Disabled";
        public static final String ONE_LINE = "1 Line";
        public static final String TWO_LINES = "2 Lines";
        public static final String THREE_LINES = "3 Lines";
        public static final String FOUR_LINES = "4 Lines";
    }

    public String getTextPreviewValue() {
        return findSetting(textPreviewField).getText();
    }

    public MailSettingsPage setTextPreview(String value) {
        findSetting(textPreviewField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class VolumeKeyBehavior {
        public static final String EMAIL_NAVIGATION = "Email navigation";
        public static final String VOLUME_CONTROL = "Volume control";
    }

    public String getVolumeKeyBehaviorValue() {
        return findSetting(volumeKeyBehaviorField).getText();
    }

    public MailSettingsPage setVolumeKeyBehavior(String value) {
        findSetting(volumeKeyBehaviorField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class SilentInterval {
        public static final String DO_NOT_SKIP = "Do not skip notifications";
        public static final String ONE_MINUTE = "1 minute";
        public static final String THREE_MINUTES = "3 minutes";
        public static final String FIVE_MINUTES = "5 minutes";
    }

    public String getSilentIntervalBetweenNotficationsValue() {
        return findSetting(silentIntervalBetweenNotificationsField).getText();
    }

    public MailSettingsPage setSilentIntervalBetweenNotfications(String value) {
        findSetting(silentIntervalBetweenNotificationsField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class MailNotifications {
        public static final String ALL = "All";
        public static final String VIP_ONLY = "VIP only";
        public static final String NONE = "None";
    }

    public String getMailNotificationsValue() {
        return findSetting(mailNotificationsField).getText();
    }

    public MailSettingsPage setMailNotifications(String value) {
        findSetting(mailNotificationsField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public String getSoundVaValue() {
        return findSetting(soundField).getText();
    }

    /**
     * Sets sound setting.
     * @param value is device dependant, different values can be available on different devices
     * @return this page
     */
    public MailSettingsPage setSound(String value) {
        findSetting(soundField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public class Vibrate {
        public static final String ALWAYS = "Always";
        public static final String WHEN_IN_SILENT_MODE = "When in silent mode";
        public static final String NEVER = "Never";
    }

    public String getVibrateValue() {
        return findSetting(vibrateField).getText();
    }

    public MailSettingsPage setVibrate(String value) {
        findSetting(vibrateField).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();
        return this;
    }

    public String getBuildVersionValue() {
        return findSetting(buildVersionField).getText();
    }

    public String getEasDeviceIdValue() {
        return findSetting(easDeviceIdField).getText();
    }

    //########################################
    //Buttons
    //########################################

    public List<String> getListOfEmailAliases() {
        findSetting(emailAliasesButton).click();
        ArrayList<String> aliases = new ArrayList<>();
        String lastElement = null;
        while (true) {
            List webElements = driver.findElements(emailAliasTextView);
            for (Object w : driver.findElements(emailAliasTextView)) {
                String text = ((WebElement) w).getText();
                if (!aliases.contains(text)) {
                    aliases.add(text);
                }
            }

            if (aliases.isEmpty()
                    || lastElement != null
                    && !lastElement.equals(((WebElement) webElements.get(webElements.size() - 1)).getText())) {
                if (webElements.size() == 0) break;
                lastElement = ((WebElement) webElements.get(webElements.size() - 1)).getText();
                scroll(DIRECTION_DOWN);
            } else {
                //if last visible alias equals to last list member
                break;
            }
        }
        goBack();
        return aliases;
    }

    public MailSettingsPage addEmailAlias(String alias) {
        findSetting(emailAliasesButton).click();
        waitFor(addEmailAliasesButton).click();
        typeIn(By.xpath("//android.widget.EditText"), alias);
        closeDialogWithPositiveButton();
        goBack();
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage removeEmailAlias(String alias) {
        findSetting(emailAliasesButton).click();
        waitFor(deleteEmailButtonByAlias(alias)).click();
        goBack();
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage removeAllEmailAliases() {
        findSetting(emailAliasesButton).click();
        while (isPresent(deleteAliasButton)) {
            waitFor(deleteAliasButton).click();
        }
        goBack();
        return waitForPageToBeLoaded();
    }

    public class OutOfOfficeValues {
        public static final String IN_THE_OFFICE = "In the office";
        public static final String OUT_OF_THE_OFFICE = "Out of the office";
        public static final String OUT_OF_THE_OFFICE_DURING_TIME = "Out of the office during time";
    }

    public MailSettingsPage setOutOfOffice(String value, String message, String startDate, String startTime, String stopDate, String stopTime,
                                           boolean replyToKnownUsers, boolean replyAllUsers) {
        findSetting(outOfOfficeButton).click();
        waitFor(outOfOfficeSpinner).click();
        waitFor(byClassAndText(CHECKED_TEXT_VIEW, value)).click();

        switch (value) {
            case OutOfOfficeValues.IN_THE_OFFICE :
                // do nothing
                break;
            case OutOfOfficeValues.OUT_OF_THE_OFFICE_DURING_TIME:
                if (startDate != null && !startDate.isEmpty()) {
                    waitFor(outOfOfficeStartDateButton).click();
                    setPickerDate(startDate);
                }
                if (startTime != null && !startTime.isEmpty()) {
                    waitFor(outOfOfficeStartTimeButton).click();
                    setPickerTime(startTime.split(":")[0], startTime.split(":")[1]);
                }
                if (stopDate != null && !stopDate.isEmpty()) {
                    waitFor(outOfOfficeEndDateButton).click();
                    setPickerDate(stopDate);
                }
                if (stopTime != null && !stopTime.isEmpty()) {
                    waitFor(outOfOfficeEndTimeButton).click();
                    setPickerTime(stopTime.split(":")[0], stopTime.split(":")[1]);
                }
            case OutOfOfficeValues.OUT_OF_THE_OFFICE :
                if (message != null) {
                    typeIn(outOfOfficeEditText, message);
                    tryHideKeyboard();
                }
                if (replyToKnownUsers) {
                    waitFor(outOfOfficeKnownUsers).click();
                }
                if (replyAllUsers) {
                    waitFor(outOfOfficeUnknownUsers).click();
                }
                break;
        }

        waitFor(saveOutOfOfficeButton).click();
        return waitForPageToBeLoaded();
    }

    public String getOutOfOfficeValue() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeSpinnerTextView).getText();
    }

    public String getOutOfOfficeStartDate() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeStartDateButton).getText();
    }

    public String getOutOfOfficeStartTime() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeStartTimeButton).getText();
    }

    public String getOutOfOfficeEndDate() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeEndDateButton).getText();
    }

    public String getOutOfOfficeEndTime() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeEndTimeButton).getText();
    }

    public String getOutOfOfficeMessage() {
        findSetting(outOfOfficeButton).click();
        return waitFor(outOfOfficeEditText).getText();
    }

    public MailSettingsPage pressServerSettingsButton() {
        findSetting(serverSettingsButton).click();
        // TODO: 10/25/16 add server settings
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage pressKeyStoreButton() {
        findSetting(keystoreButton).click();
        // TODO: 10/25/16 add keystore settings
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage pressMailSecurity() {
        findSetting(mailSecurity).click();
        // TODO: 10/25/16 add keystore settings
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage restoreDefaults() {
        findSetting(restoreDefaultsButton).click();
        closeDialogWithPositiveButton();
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage openTermsOfServices() {
        findSetting(termsOfServiceButton).click();
        return waitForPageToBeLoaded();
    }

    //########################################
    //Switches
    //########################################

    private boolean getSwitchState(By switchId) {
        return findSetting(switchId).getText().equals("ON");
    }

    private MailSettingsPage turnSwitch(By locator, boolean turnOn) {
        findSetting(locator);
        if (getSwitchState(locator) != turnOn) {
            waitFor(locator).click();
        }
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage switchConfirmBeforeDeleting(boolean turnOn) {
        return turnSwitch(confirmBeforeDeletingSwitch, turnOn);
    }

    public MailSettingsPage switchUseSmartSend(boolean turnOn) {
        return turnSwitch(useSmartSendSwitch, turnOn);
    }

    public MailSettingsPage switchSignInAutoRetry(boolean turnOn) {
        return turnSwitch(signInAutoRetrySwitch, turnOn);
    }

    public MailSettingsPage switchEmailThreading(boolean turnOn) {
        return turnSwitch(emailThreadingSwitch, turnOn);
    }

    public MailSettingsPage switchSwipeAction(boolean turnOn) {
        return turnSwitch(swipeActionsSwitch, turnOn);
    }

    public MailSettingsPage switchDetailedNotifications(boolean turnOn) {
        return turnSwitch(detailedNotificationsSwitch, turnOn);
    }

    public MailSettingsPage switchEmailNotifications(boolean turnOn) {
        return turnSwitch(emailSentNotificationSwitch, turnOn);
    }
}
