package com.mobileiron.appiumtest.pageObjects.contacts;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.pageObjects.calendar.CalendarPage;
import com.mobileiron.appiumtest.pageObjects.email.MailsListPage;
import com.mobileiron.appiumtest.util.Helpers;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by tlozovyi on 10/21/16.
 */

public class ContactDetailsPage extends DefaultPage{
    private ContactDetailsPage(AndroidDriver driver) {
        super(driver);
    }

    public static ContactDetailsPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + MailsListPage.class.getSimpleName());
        Helpers.delay(PAGE_LOADING_TIMEOUT);
        return new ContactDetailsPage(driver);
    }

    @Override
    protected String expectedActivityName() {
        // TODO: 10/21/16
        return null;
    }

    @Override
    public ContactDetailsPage waitForPageToBeLoaded() {
        // TODO: 10/21/16
        return null;
    }

    @Override
    public boolean isPageLoaded() {
        // TODO: 10/21/16
        return false;
    }
}
