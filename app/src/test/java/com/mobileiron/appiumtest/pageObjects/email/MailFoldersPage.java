package com.mobileiron.appiumtest.pageObjects.email;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.util.Helpers;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;

public class MailFoldersPage extends DefaultPage {

    private static By pageTitle = byId("title");
    private static By homeButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");

    //toolbar
    private static By moreOptionsButton = By.xpath("//android.support.v7.widget.LinearLayoutCompat/android.widget.ImageView");
    private static By refreshButton = byClassAndText(TEXT_VIEW, "Refresh");
    private static By settingsButton = byClassAndText(TEXT_VIEW, "Settings");

    private static By composeButton = byId("floating_btn");

    //folder list element content
    private static By mailboxName = byId("mailbox_name");
    private static By newMessageCount = byId("new_message_count");
    private static By syncButton = byId("sync");
    private static By readIndicatorChip = byId("chip");


    private static By folderByName(String name) {
        return By.xpath("//*[@resource-id='" + RES_PREFIX + "mailbox_name' and contains(@text, '" + name +"')]");
    }

    private MailFoldersPage(AndroidDriver driver) {
        super(driver);
    }

    public static MailFoldersPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + MailFoldersPage.class.getSimpleName());
        return new MailFoldersPage(driver).waitForPageToBeLoaded();
    }

    @Override
    protected String expectedActivityName() {
        // TODO: 10/20/16  
        return null;
    }

    @Override
    public MailFoldersPage waitForPageToBeLoaded() {
        if (waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Folders")) {
            return this;
        }
        Assert.fail();
        return null;
    }

    @Override
    public boolean isPageLoaded() {
        return waitFor(pageTitle, PAGE_LOADING_TIMEOUT).getText().equals("Folders");
    }

    public MailsListPage openFolder(String name) {
        writeLog("Opening folder: " + name);
        findBelow(folderByName(name)).click();
        return MailsListPage.getInstance(driver);
    }

    private List<String> getFolders(By locator) {
        List<String> names = new ArrayList<>();
        String lastName = null;
        int scrollCount = 0;
        while (true) {
            List folders = driver.findElements(locator);
            if (folders.isEmpty()) {
                break;
            }
            for (int i = 0; i < folders.size(); i++) {
                WebElement folder = (WebElement) folders.get(i);
                String folderName = folder.getText();
                if (!names.contains(folderName)) {
                    names.add(folderName);
                }
            }
            String lastElementName = ((WebElement) folders.get(folders.size() - 1)).getText();
            if (lastElementName == null || lastElementName.equals(lastName)) {
                break;
            }
            lastName = lastElementName;
            scroll(DIRECTION_DOWN);
            scrollCount++;
        }
        for (int i = 0; i < scrollCount; i++) {
            scroll(DIRECTION_UP);
        }
        return names;
    }

    public List<String> getAllFoldersNames() {
        return getFolders(mailboxName);
    }

    public List<String> getFoldersWithUnreadMails() {
        return getFolders(By.xpath("//*[@resource-id='com.mobileiron.android.pim:id/message_count_box']" +
                "/preceding-sibling::android.widget.LinearLayout/android.widget.TextView"));
    }

    public void pressHomeButton() {
        waitFor(homeButton).click();
    }

    public MailFoldersPage refresh() {
        waitFor(moreOptionsButton).click();
        waitFor(refreshButton).click();
        delay(5);
        return waitForPageToBeLoaded();
    }

    public MailSettingsPage openSettings() {
        waitFor(moreOptionsButton).click();
        waitFor(settingsButton).click();
        return MailSettingsPage.getInstance(driver);
    }

    public NewMailPage pressComposeButton() {
        waitFor(composeButton).click();
        return NewMailPage.getInstance(driver);
    }
}
