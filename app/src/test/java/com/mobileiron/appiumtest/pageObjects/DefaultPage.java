package com.mobileiron.appiumtest.pageObjects;

import com.mobileiron.appiumtest.util.Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidKeyMetastate;

import static com.mobileiron.appiumtest.util.Helpers.sendText;
import static org.junit.Assert.*;

/**
 * Created by tlozovyi on 10/18/16.
 */

public abstract class DefaultPage {
    public static final String PACKAGE = "com.mobileiron.android.pim";
    public static final String RES_PREFIX = PACKAGE + ":id/";
    public static final String ANDROID_RES = "android:id/";

    public static final int DIRECTION_LEFT = 0;
    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_DOWN = 3;

    public static final int METHOD_FULL = 0;
    public static final int METHOD_ONE_BY_ONE = 1;
    public static final int INVALID_VALUE = -1;

    public static final int DEFAULT_TIMEOUT = 5;
    public static final int PAGE_LOADING_TIMEOUT = 10;

    //classes
    public static final String TEXT_VIEW = "android.widget.TextView";
    public static final String CHECKED_TEXT_VIEW = "android.widget.CheckedTextView";

    protected static By byId(String id) {
        return By.id(RES_PREFIX + id);
    }

    protected static By byAndroidId(String id) {
        return By.id(ANDROID_RES + id);
    }

    protected static By byClassAndText(String widgetClass, String text) {
        return By.xpath("//" + widgetClass + "[@text=\"" + text + "\"]");
    }

    //buttons
    private static By positiveAndroidDialogButton = By.id(ANDROID_RES + "button1");
    private static By negativeAndroidDialogButton = By.id(ANDROID_RES + "button2");
    private static By neutralAndroidDialogButton = By.id(ANDROID_RES + "button3");

    protected static By elementOfDialogList(String name) {
        return By.xpath("//android.widget.TextView[@resource-id='android:id/text1' and contains(@text,'" + name + "')]");
    }

    protected AndroidDriver driver;

    protected DefaultPage(){}

    protected DefaultPage (AndroidDriver driver) {
        this.driver = driver;

        // TODO: 10/19/16 uncomment when activities will be known
//        // Making sure we are on correct activity:
//        if (!isExpectedActivity(driver.currentActivity())) {
//            // Waiting for a few secs and failing if we are still not there:
//            delay(10);
//            String activity = driver.currentActivity();
//            if (isExpectedActivity(activity)) {
//                return;
//            } else if (expectedActivityName() != null) {
//                Assert.assertEquals(expectedActivityName(), activity);
//            } else {
//                Assert.assertTrue("Unexpected activity: " + activity, isExpectedActivity(activity));
//            }
//        }
    }

    protected boolean isExpectedActivity(String activityName) {
        return expectedActivityName() == null || expectedActivityName().equals(activityName);
    }

    protected abstract String expectedActivityName();

    public abstract DefaultPage waitForPageToBeLoaded();

    public abstract boolean isPageLoaded();

    //dialogs
    public DefaultPage closeDialogWithNegativeButton() {
        writeLog("Closing dialog with negative button");
        if (isPresent(negativeAndroidDialogButton)) {
            driver.findElement(negativeAndroidDialogButton).click();
        }
        return this;
    }

    public DefaultPage closeDialogWithPositiveButton() {
        writeLog("Closing dialog with positive button");
        if (isPresent(positiveAndroidDialogButton)) {
            driver.findElement(positiveAndroidDialogButton).click();
        }
        return this;
    }

    public DefaultPage closeDialogWithNeutralButton() {
        writeLog("Closing dialog with neutral button");
        if (isPresent(neutralAndroidDialogButton)) {
            driver.findElement(neutralAndroidDialogButton).click();
        }
        return this;
    }


    protected boolean isPresent(By locator) {
        List elements = driver.findElements(locator);
        return !elements.isEmpty();
    }

    protected void delay(int seconds) {
        Helpers.delay(seconds);
    }

    protected WebElement waitFor(By locator) {
        return waitFor(locator, DEFAULT_TIMEOUT);
    }

    protected WebElement waitFor(By locator, int timeOutInSeconds) {
        new WebDriverWait(driver, timeOutInSeconds).until(
                ExpectedConditions.presenceOfElementLocated(locator));
        return (WebElement) driver.findElements(locator).get(0);
    }

    protected WebElement findAndTypeIn(String name, String text, boolean forceClear) {
        scrollTo(name);
        return typeIn(By.name(name), text, forceClear);
    }

    protected WebElement typeIn(By locator, String text) {
        return typeIn(locator, text, false);
    }

    protected WebElement typeIn(By locator, String text, boolean forceClear) {
        WebElement element = driver.findElement(locator);
        element.click();
        if (forceClear) {
            forceClear(element);
        }
        sendText(text);
        return element;
    }

    protected WebElement findByName(String name) {
        By by = By.name(name);
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException e) {
            tryHideKeyboard();
            scrollTo(name);
            return driver.findElement(by);
        }
    }

    public void scroll(int direction) {
        scroll(direction, INVALID_VALUE, INVALID_VALUE);
    }

    public void scroll(int direction, int x, int y) {
        scroll(direction, x, y, INVALID_VALUE, INVALID_VALUE);
    }

    // TODO: 10/19/16 check method
    public void scroll(int direction, int x, int y, int dx, int dy) {
        Dimension dimensions = driver.manage().window().getSize();
        int height = dimensions.getHeight();
        int width = dimensions.getWidth();

        double scrollStartX = width * 0.5;
        double scrollEndX = width * 0.5;
        double scrollStartY = height * 0.5;
        double scrollEndY = height * 0.5;

        int directionMultiplier = 1; // direction will define the sign of diff vars
        switch (direction) {
            case DIRECTION_LEFT:
                scrollEndX = width * 0.8;
                break;

            case DIRECTION_UP:
                scrollEndY = height * 0.8;
                break;

            case DIRECTION_RIGHT:
                scrollEndX = width * 0.2;
                directionMultiplier = -1;
                break;

            case DIRECTION_DOWN:
                scrollEndY = height * 0.2;
                directionMultiplier = -1;
                break;
        }
        int diffX = (int) (scrollEndX - scrollStartX);
        int diffY = (int) (scrollEndY - scrollStartY);
        if (dx != INVALID_VALUE) {
            diffX = directionMultiplier * dx;
        }
        if (dy != INVALID_VALUE) {
            diffY = directionMultiplier * dy;
        }
        int startX = (int) scrollStartX;
        int startY = (int) scrollStartY;
        if (x > INVALID_VALUE && y > INVALID_VALUE) {
            startX = x;
            startY = y;
        }
        int endX = Math.max(startX + diffX, 1);
        int endY = Math.max(startY + diffY, 1);
        driver.swipe(startX, startY, endX, endY, 1200);
    }

    // TODO: 10/19/16 check that code below
    public void scrollTo(String name) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))." +
                "scrollIntoView(new UiSelector().textContains(\""+ name +"\").instance(0))");
    }

    public void forceClear(By locator) {
        forceClear(driver.findElement(locator));
    }

    public void forceClear(WebElement field) {
        field.clear();

//        field.click();
//        driver.pressKeyCode(AndroidKeyCode.KEYCODE_MOVE_END, AndroidKeyMetastate.META_CTRL_ON);
//
//        int length = field.getText().length();
//        while (field.getText().length() != 0) {
//            driver.longPressKeyCode(AndroidKeyCode.DEL);
//            if (length == field.getText().length()) {
//                //situation when editText contains hint, which is parsed as text
//                break;
//            }
//            length = field.getText().length();
//        }
    }

    public boolean isBelow(By locator, int y) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            if (element.getLocation().y > y) {
                return true;
            }
        }
        return false;
    }

    public boolean isAbove(By locator, int y) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            if (element.getLocation().y < y) {
                return true;
            }
        }
        return false;
    }

    public boolean isBetween(By locator, int startY, int endY) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            int y = element.getLocation().y;
            if (y > startY && y < endY) {
                return true;
            }
        }
        return false;
    }

    public WebElement findBelow(By by) {
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException ex) {
            tryHideKeyboard();
            // Scrolling down a few times, if still unable find - failing
            for (int i = 0; i < 5; ++i) {
                WebElement e = tryFind(by);
                if (e != null) {
                    return e;
                }
                scroll(DIRECTION_DOWN);
            }
        }
        return driver.findElement(by);
    }

    public WebElement tryFind(By by) {
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public boolean tryClick(WebElement element) {
        if (element != null) {
            element.click();
        }
        return element != null;
    }

    public boolean tryHideKeyboard() {
        writeLog("Trying to hide virtual keyboard");
        try {
            driver.hideKeyboard();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void longTap(WebElement element) {
        new TouchAction(driver).longPress(element).release().perform();
    }


    public DefaultPage goBack() {
        tryHideKeyboard();
        pressBackButton();
        delay(1);
        return this;
    }

    public DefaultPage pressBackButton() {
        writeLog("Pressing Back button");
        driver.pressKeyCode(AndroidKeyCode.BACK);
        return this;
    }

    public void writeLog(String message) {
        Helpers.writeLog(message);
    }

    // TODO: 10/19/16 check method
    protected void waitInActivity(String activityName, int sec) {
        for (int i = 0; i < sec; ++i) {
            if (activityName.equals(driver.currentActivity())) {
                return;
            }
            delay(1);
        }
    }

    // TODO: 10/19/16 check method
    public void openMenu() {
        delay(5);
        driver.pressKeyCode(AndroidKeyCode.MENU);
        delay(5);
    }

    /**
     *
     * @param date in format 01 October 2016
     */
    public void setPickerDate(String date){
        String[] parsed = date.split(" ");
        String year = parsed[parsed.length - 1];
        WebElement yearElement = waitFor(byAndroidId("date_picker_year"));
        if (!yearElement.getText().equals(year)) {
            yearElement.click();
            while (!isPresent(byClassAndText(TEXT_VIEW, year)) ) {
                WebElement centerYear = waitFor(By.xpath("//android.widget.ListView/*[@index='2']"));
                Pair centerPosition = getElementCenter(centerYear);
                boolean scrollUp = Integer.parseInt(year) > Integer.parseInt(centerYear.getText());
                scroll(scrollUp ? DIRECTION_DOWN : DIRECTION_UP, centerPosition.x, centerPosition.y);
            }
            waitFor(byClassAndText(TEXT_VIEW, year)).click();
        }

        WebElement listView = waitFor(By.xpath("//android.widget.ListView"));
        Pair listPosition = getElementCenter(listView);
        boolean scrollUp = true;
        while (!isPresent(By.xpath("//*[contains(@content-desc,'" + date + "')]"))) {
            if (scrollUp && driver.findElements(byClassAndText(TEXT_VIEW, "01 January 2016")).isEmpty()) {
                scrollUp = false;
            }
            scroll(scrollUp ? DIRECTION_DOWN : DIRECTION_UP, listPosition.x, listPosition.y);
        }
        waitFor(By.xpath("//*[contains(@content-desc,'" + date + "')]")).click();
        closeDialogWithPositiveButton();
    }

    /**
     *
     * @param hours in format 1, 9, 12, 24
     * @param minutes in format 0, 5, 30, 55
     */
    public void setPickerTime(String hours, String minutes){
        int m = Integer.parseInt(minutes);
        int h = Integer.parseInt(hours);
        if (m > 59 || h > 23) fail();

        if (waitFor(byClassAndText(CHECKED_TEXT_VIEW, "AM")) == null) {
            waitFor(byAndroidId("hours")).click();
            waitFor(By.xpath("//*[contains(@content-desc,'" + hours + "')]")).click();
        } else {
            WebElement amElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "AM"));
            WebElement pmElement = waitFor(byClassAndText(CHECKED_TEXT_VIEW, "PM"));
            if (h == 0) {
                amElement.click();
                waitFor(By.xpath("//*[contains(@content-desc,'" + 12 + "')]")).click();
            } else if (h < 12) {
                amElement.click();
                waitFor(By.xpath("//*[contains(@content-desc,'" + h + "')]")).click();
            } else {
                pmElement.click();
                waitFor(By.xpath("//*[contains(@content-desc,'" + (h - 12) + "')]")).click();
            }
        }

        delay(2);


        if (m % 5 == 0) {
            waitFor(By.xpath("//*[contains(@content-desc,'" + m + "')]")).click();
        } else {
            int previous = m - m % 5;
            int next = previous < 55 ? previous + 5 : 0;

            WebElement prevElement = waitFor(By.xpath("//*[contains(@content-desc,'" + previous + "')]"));
            WebElement nextElement = waitFor(By.xpath("//*[contains(@content-desc,'" + next + "')]"));

            Pair prevCenter = getElementCenter(prevElement);
            Pair nextCenter = getElementCenter(nextElement);

            double distance = Math.sqrt(Math.pow(prevCenter.x - nextCenter.x, 2) + Math.pow(prevCenter.y - nextCenter.y, 2));
            for (int i = 1; i <= 10; i++) {
                double partDistance =  i * distance / 10;
                double mult = partDistance / distance;
                Pair tapPoint = new Pair((int)(prevCenter.x + (nextCenter.x - prevCenter.x) * mult)
                        , (int)(prevCenter.y + (nextCenter.y - prevCenter.y) * mult));
                driver.tap(1, tapPoint.x, tapPoint.y, 1);
                if (driver.findElement(byAndroidId("minutes")).getText().equals(minutes)) {
                    break;
                }
            }
        }
        closeDialogWithPositiveButton();
        closeDialogWithPositiveButton(); //second dialog with half hour equality message
    }

    private Pair getElementCenter(WebElement element) {
        Point p = ((Locatable)element).getCoordinates().onPage();
        int x = p.getX();
        int y = p.getY();
        int height = element.getSize().getHeight();
        int width = element.getSize().getWidth();
        int centerX = x + (width / 2);
        int centerY = y + (height / 2);
        return  new Pair(centerX, centerY);
    }

    private class Pair {
        public int x,y;

        public Pair (int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "x: " + x + " y: " + y;
        }
    }

    /**
     * Set timezone.
     *
     * @param timezone to be set
     * @return self
     */
    public DefaultPage setTimezone(String timezone, By timeZoneUpperBound, By timeZoneLowerBound) {
//        By timeZoneLocator = By.xpath(String.format(TIME_ZONE_FORMATTER, timezone));
        // FIXME: 10/24/16
        By timeZoneLocator = null;
        // scroll up, to upper bound then down
        boolean found = false;
        while (true) {
            if (isPresent(timeZoneLocator)) {
                found = true;
                break;
            }

            if (isPresent(timeZoneUpperBound)) {
                break;
            }

            scroll(DIRECTION_UP);
        }

        if (!found) {
            while (true) {
                if (isPresent(timeZoneLocator)) {
                    break;
                }

                if (isPresent(timeZoneLowerBound)) {
                    break;
                }

                scroll(DIRECTION_DOWN);
            }
        }

        driver.findElement(timeZoneLocator).click();

        return this;
    }

    public void rotate(ScreenOrientation screenOrientation) {
        driver.rotate(screenOrientation);
    }

    protected void scrollToBottom(By locator) {
        String lastTitle = null;
        while (true) {
            List elements = driver.findElements(locator);
            String lastElementTitle ;
            if (elements.isEmpty() || (lastElementTitle = ((WebElement) elements.get(elements.size() - 1)).getText()).equals(lastTitle)) {
                break;
            }
            scroll(DIRECTION_DOWN);
            lastTitle = lastElementTitle;
        }
    }
}
