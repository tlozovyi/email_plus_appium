package com.mobileiron.appiumtest.pageObjects.email;

import com.mobileiron.appiumtest.pageObjects.DefaultPage;
import com.mobileiron.appiumtest.pageObjects.calendar.CalendarPage;
import com.mobileiron.appiumtest.pageObjects.contacts.ContactDetailsPage;
import com.mobileiron.appiumtest.util.Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by tlozovyi on 10/21/16.
 */

public class ViewMailPage extends DefaultPage {

    // button locators
    private static By moreOptionsButton = By.xpath("//ImageButton[@content-desc='More options']");
    private static By favoriteButton = By.id("com.mobileiron.android.pim:id/favorite");
    private static By replyButton = By.id("com.mobileiron.android.pim:id/reply");
    private static By replyAllButton = By.id("com.mobileiron.android.pim:id/reply_all");
    private static By forwardButton = By.id("com.mobileiron.android.pim:id/btn_forward");
    private static By deleteButton = By.id("com.mobileiron.android.pim:id/delete");
    private static By moveButton = By.id("com.mobileiron.android.pim:id/mv_move");
    private static By homeButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");

    //intermail navigation bar
    private static By moveToNewerNotifier = byId("moveToNewer");
    private static By pageNotifier = byId("pageNotifier");
    private static By moveToOlderNotifier = byId("moveToOlder");

    //send-receive info
    private static By senderImage = byId("badge");
    private static By senderName = byId("from_name");
    private static By senderEmail = byId("from_address");
    private static By openDetailsButton = byId("show_details");
    private static By sendDate = byId("date");
    private static By recipientEmail = By.xpath("//*[@resource-id='com.mobileiron.android.pim:id/tos']/*[@index='1']");

    //invite to calendar
    private static By viewInCalendarButton = byId("invite_link");
    private static By calendarEventTitle = byId("mail_invite_event_title");
    private static By inviteDate = byId("mail_invite_event_daterange");
    private static By acceptEventButton = byId("accept");
    private static By maybeEventButton = byId("maybe");
    private static By declineEventButton = byId("decline");

    //attachments
    private static By attachmentIcon = byId("attachment_icon");
    private static By attachmentName = byId("attachment_name");
    private static By attachmentSize = byId("attachment_info");
    private static By downloadAttachmentButton = byId("download");

    // menu option locators
    private static By readButton = byClassAndText(TEXT_VIEW, "Mark as unread");

    // complex locators
    private static By mailSubject() {
        return By.xpath("//android.widget.TextView[@resource-id='com.mobileiron.android.pim:id/subject']");
    }

    private static By mailFromName() {
        return By.xpath("//android.widget.TextView[@resource-id='com.mobileiron.android.pim:id/from_name']");
    }

    private static By mailFromAddress() {
        return By.xpath("//android.widget.TextView[@resource-id='com.mobileiron.android.pim:id/from_address']");
    }

    private static By mailToAddresses() {
        return By.xpath("//android.widget.TextView[@resource-id='com.mobileiron.android.pim:id/addresses']");
    }

    private ViewMailPage(AndroidDriver driver) {
        super(driver);
    }

    public static ViewMailPage getInstance(AndroidDriver driver) {
        Helpers.writeLog("Loading " + ViewMailPage.class.getSimpleName());
        return new ViewMailPage(driver).waitForPageToBeLoaded();
    }

    @Override
    protected String expectedActivityName() {
        // TODO: 10/21/16
        return null;
    }

    @Override
    public ViewMailPage waitForPageToBeLoaded() {
        waitFor(pageNotifier, DEFAULT_TIMEOUT);
        return this;
    }

    @Override
    public boolean isPageLoaded() {
        return isPresent(pageNotifier);
    }

    public int getNumberOfAttachments() {
        return driver.findElements(attachmentIcon).size();
    }

    private void openMoreOptionsMenu() {
        driver.findElement(moreOptionsButton).click();
    }

    public void pressHomeButton() {
        driver.findElement(homeButton).click();
    }

    public boolean hasToAddress(String to) {
        return waitFor(mailToAddresses()).getText().equals(to);
    }

    public boolean hasFromAddress(String fromAddress) {
        return waitFor(mailFromAddress()).getText().equals(fromAddress);
    }

    public boolean hasFromName(String fromName) {
        return waitFor(mailFromName()).getText().equals(fromName);
    }

    public boolean hasSubject(String subject) {
        return waitFor(mailSubject()).getText().equals(subject);
    }

    public String getMessage() {
        List elements = driver.findElements(By.xpath("//android.webkit.WebView/android.webkit.WebView/android.view.View"));
        StringBuilder message = new StringBuilder();
        for (int i = 0; i < elements.size(); i++) {
            WebElement element = (WebElement) elements.get(i);
            message.append(element.getAttribute("name"));
        }
        writeLog("Message is: " + message.toString());
        return message.toString();
    }

    public boolean hasMessage(String body) {
        return getMessage().contains(body);
    }

    public ViewMailPage pressFavoritesButton() {
        waitFor(favoriteButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    public ViewMailPage swipeToNextMail() {
        scroll(DIRECTION_RIGHT);
        return this;
    }

    public ViewMailPage swipeToPreviousMail() {
        scroll(DIRECTION_LEFT);
        return this;
    }

    /**
     * @return invite date if mail is an invitation, or fail otherwise
     */
    public String getInviteDate() {
        return driver.findElement(inviteDate).getText();
    }

    public ViewMailPage delete() {
        waitFor(deleteButton, DEFAULT_TIMEOUT).click();
        return this;
    }

    /**
     * Emulates press of VolumeUp hard button.
     * Opens next mail. Works only when this feature is turned on in settings.
     * @return
     */
    public ViewMailPage pressVolumeUp() {
        driver.pressKeyCode(AndroidKeyCode.KEYCODE_VOLUME_UP);
        return this;
    }

    /**
     * Emulates press of VolumeDown hard button.
     * Opens previous mail. Works only when this feature is turned on in settings.
     * @return
     */
    public ViewMailPage pressVolumeDown() {
        driver.pressKeyCode(AndroidKeyCode.KEYCODE_VOLUME_DOWN);
        return this;
    }

    @Override
    public MailsListPage pressBackButton() {
        super.pressBackButton();
        return MailsListPage.getInstance(driver);
    }

    @Override
    public MailsListPage goBack() {
        super.goBack();
        return MailsListPage.getInstance(driver);
    }

    public NewMailPage reply() {
        waitFor(replyButton, DEFAULT_TIMEOUT).click();
        return NewMailPage.getInstance(driver);
    }

    public NewMailPage replyAll() {
        waitFor(replyAllButton, DEFAULT_TIMEOUT).click();
        return NewMailPage.getInstance(driver);
    }

    public NewMailPage forward() {
        waitFor(forwardButton, DEFAULT_TIMEOUT).click();
        return NewMailPage.getInstance(driver);
    }

    public ViewMailPage openEmailDetailes() {
        waitFor(openDetailsButton).click();
        return this;
    }

    public CalendarPage viewInCalendar() {
        driver.findElement(viewInCalendarButton).click();
        return CalendarPage.getInstance(driver);
    }

    public ContactDetailsPage tapRecipientAddress() {
        driver.findElement(recipientEmail).click();
        return ContactDetailsPage.getInstance(driver);
    }
}
