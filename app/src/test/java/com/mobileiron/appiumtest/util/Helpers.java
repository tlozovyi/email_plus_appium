package com.mobileiron.appiumtest.util;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.Executor;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;

public class Helpers {

    private Helpers() {
    }

    public static String executeCommand(String command) {
        StringBuilder output = new StringBuilder();

        assert command != null;

        Process process;
        try {
            Runtime runtime = Runtime.getRuntime();
            process = runtime.exec(command);
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine())!= null) {
                output.append(line).append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }

    public static void executeCommandApach(String command) {
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        Executor executor = new DefaultExecutor();
        executor.setExitValue(1);
        try {
            executor.execute(new CommandLine(command), resultHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendText(String text) {
        text = text.replace(" ", "%s");
        String command = String.format("adb shell input text '%s'", text);
        executeCommand(command);
    }

    public static void delay(int seconds) {
        writeLog("Delay for " + seconds + " seconds");
        try {
            Thread.sleep(seconds * TimeUnit.SECONDS.toMillis(1));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void activeWait(AndroidDriver driver, int seconds) {
        String impossibleId = "" + new Random().nextInt();
        try {
            WebDriverWait driverWait = new WebDriverWait(driver, seconds);
            driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(impossibleId)));
        } catch (TimeoutException e) {
            // this will definitely happen
        }
    }

    public static void writeLog(String message) {
        System.out.println(message);
    }

}